#!/bin/bash

cd /home/HGTD-180-ProbeStation/hgtd-mpi-qcts-autoprobe/

wget -O data_cleanroom_environment.txt http://raspberrypi10clic.cern.ch/environment_monitor/0.html
wget -O data_cleanroom_particles.txt http://raspberrypi10clic.cern.ch/particle_monitor/0.html
