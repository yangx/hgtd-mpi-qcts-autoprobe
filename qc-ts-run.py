#!/usr/local/bin/python3.11

# from sentio_prober_control.Communication.CommunicatorTcpIp import CommunicatorTcpIp
from sentio_prober_control.Sentio.ProberSentio import *
from sentio_prober_control.Sentio.Enumerations import *

from sentio_prober_control.Communication.CommunicatorGpib import *
from sentio_prober_control.Communication.CommunicatorTcpIp import *

import os
import subprocess
import os.path
import cv2
import numpy as np
import time
import imutils
from math import *
from datetime import datetime

from hgtd_wafermap_data import dieMap, WaferHolders


#### Global Config #####

base_path = "/home/HGTD-180-ProbeStation/hgtd-mpi-qcts-autoprobe/imgs/"
# mpi_addr = "128.141.179.86"
mpi_addr = "128.141.179.106"
mpi_img_path = "C:\\Users\\MPI\\Downloads\\nginx-1.24.0\html\qc-ts-img"

#holder_id = 'holder_H1_20240320' ## Current one
# holder_id = 'holder_H1_20240423_IHEP_UBMed_Irradiated' ## Current one
#holder_id = 'holder_H1_20240506_USTC_UBMed' ## Current one
holder_id = 'holder_H2_20240611_IHEP_UBMed' ## Current one

log_file = './log_probe_station_auto_test.txt'




HolderPositionMap = WaferHolders[holder_id]
#vendor = "USTC"
vendor = "IHEP"
#lumi=12
# lumi=6 #USTC UBMed
lumi=10 #IHEP UBMed

#### Global Config #####



### Initialization ######
pixelsize = 100.0/64 # 100um == 64 pixel
comm = False
prober = False
ndie = -1

test_batch = int((time.mktime((datetime.now()).timetuple())))
test_device = None
picfolder_name = "DEFAULT_CERN"
ref_img = "img_ref.jpg"
die_pos_now = "_"
### Initialization ######
record_file = f'./job_record/job_record_{test_batch}.txt'

if vendor == "USTC":
    ref_img = "img_ref_USTC_UBMed.jpg"

if vendor == "IHEP":
    #ref_img = "img_ref_IHEP_nonUBM.jpg"
    ref_img = "img_ref_IHEP_UBMed_new.jpg"




lumi_now=lumi
# lumi=20

time_watching = time.time()
time_watching_test = time.time()

def log_info(info):
    global time_watching
    ts =  str(datetime.now())
    time_now = time.time()
    time_interval = round(time_now - time_watching,3)
    time_watching = time_now
    os.system(f'echo [{test_batch} - {ts} - i:{time_interval}s] {info}>> {log_file}')
    print(f'[LOG] [{ts} - i:{time_interval}s] {info}')
    
def record_test(info):
    global time_watching_test
    ts =  str(datetime.now())
    time_now = time.time()
    time_interval = round(time_now - time_watching_test,3)
    time_watching_test = time_now
    os.system(f'echo [{ts} - {time_interval}s] {info}>> {record_file}')
    print(f'[RECORD] [{ts} - i:{time_interval}s] {info}')
    
def check_history(col,row):
    global record_file

    if not os.path.isfile(record_file):
        return False
    
    output = subprocess.check_output(f'cat {record_file}|grep "] {col},{row},1" |wc -l ', shell=True).strip()

    if output == b'1':
        return True
    elif output == b'0':
        return False
    else:
        print(f"Unknow output:{output}, please be careful")
        return False
            


def getPID(nrow, ncol):
    for i in dieMap:
        if dieMap[i] == [nrow,ncol]:
            return i
    
    return 99


def align():

    time.sleep(0.5)
    os.system("wget -q -O "+base_path+"/img_now.jpg http://"+mpi_addr+"/qc-ts-img/img_now.jpg")

    src1_path = os.path.join(base_path, ref_img)
    src2_path = os.path.join(base_path, "img_now.jpg")
    src1 = cv2.imread(src1_path)
    src2 = cv2.imread(src2_path)
    src1 = src1[0:src1.shape[0],0:src1.shape[1]]
    src2 = src2[0:src2.shape[0],0:src2.shape[1]]

    # src1 = src1[100:src1.shape[0]-100,0:src1.shape[1]]
    # src2 = src2[100:src2.shape[0]-100,0:src2.shape[1]]
    # print(src1)
    # print(src2)

    add_img = cv2.addWeighted(src1, 0.5, src2, 0.5, 1)  

    src1 = np.float32(src1)
    src2 = np.float32(src2)
    src1 = cv2.cvtColor(src1, cv2.COLOR_BGR2GRAY)
    src2 = cv2.cvtColor(src2, cv2.COLOR_BGR2GRAY)


    start_time = time.time()
    dst = cv2.phaseCorrelate(src1,src2)
    end_time =time.time()
    print("offsetv1 = {:.4f}s".format(end_time-start_time))
    cv2.putText(add_img,str(dst[0]),(100,100),cv2.FONT_HERSHEY_COMPLEX,1, (0,255,0), 3)
    prober.move_chuck_separation()
    cv2.imwrite(base_path + "/" + "{}.jpg".format(src1_path.split("/")[-1][:-4] +"+"+ src2_path.split("/")[-1][:-4]), add_img)
    print(dst[0])
    print("shift in um:")
    print("shift  x: {:f} um, y: {:f} um".format(dst[0][0]*pixelsize, dst[0][1]*pixelsize))
    
    return (dst[0][0]*pixelsize, dst[0][1]*pixelsize)

def inRange(x,y,xrange = [0.,1.], yrange = [0.,1.]):
    if x > xrange[0] and x < xrange[1] and y > yrange[0] and y < yrange[1]:
        return True
    else:
        return False 




def align_theta():
    global lumi_now
    angleRef = 0

    log_info("start align theta")

    time.sleep(0.5)
    os.system("wget -q -O "+base_path+"/img_now_theta.jpg http://"+mpi_addr+"/qc-ts-img/img_now_theta.jpg")
    # time.sleep(0.5)

    # src1_path = os.path.join(base_path, "img_ref.jpg")
    src2_path = os.path.join(base_path, "img_now_theta.jpg")
    # src1 = cv2.imread(src1_path)
    image = cv2.imread(src2_path)

    # cv2.imshow('image', image)

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    thresh = cv2.threshold(blurred, 140, 255, cv2.THRESH_BINARY)[1]
    # thresh = cv2.threshold(blurred, 160, 255, cv2.THRESH_BINARY)[1]

    outpath="imgs/img_grey_theta.jpg"
    cv2.imwrite(outpath, thresh)

    # find contours in the thresholded image
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)

    prober.move_chuck_separation()
    cnts_sel = []
    for cnt in cnts:
        area = cv2.contourArea(cnt)
        
        if area > 2000:
            pass
            # print("debug: area for cnt", cv2.contourArea(cnt))
        
        if area > 2000 and area < 6000:
        # if area < 2100 and area > 1190:
            M = cv2.moments(cnt)
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
            

            # For USTC
            # if cX > 1000:
            #For IHEP
            matchROI = False
            matchROISub = False

            if (vendor == "USTC" and cX > 1000):
                matchROI = True

            
            if (vendor == "IHEP" and cX > 1170):
                if inRange(cX,cY,xrange = [1225.,1325.], yrange = [1380.,1460.]): matchROI = True
                if inRange(cX,cY,xrange = [1530.,1620.], yrange = [1380.,1460.]): matchROI = True
                if inRange(cX,cY,xrange = [1690.,1770.], yrange = [1380.,1460.]): matchROI = True
                if inRange(cX,cY,xrange = [1990.,2070.], yrange = [1380.,1460.]): matchROI = True
                if inRange(cX,cY,xrange = [2140.,2231.], yrange = [1380.,1460.]): matchROI = True
                if inRange(cX,cY,xrange = [2444.,2523.], yrange = [1380.,1460.]): matchROI = True
                if inRange(cX,cY,xrange = [2444.,2523.], yrange = [1380.,1460.]): matchROI = True

                if inRange(cX,cY,xrange = [1832.,1932.], yrange = [1380.,1460.]): matchROISub = True
                if inRange(cX,cY,xrange = [1382.,1472.], yrange = [1380.,1460.]): matchROISub = True


            if matchROI:
                cnts_sel.append(cnt)
                print("debug: center (matched)",cX,cY, "\t area",cv2.contourArea(cnt))
            elif matchROISub:
                cnts_sel.append(cnt)
                print("debug: center (matchedSub)",cX,cY, "\t area",cv2.contourArea(cnt))
            elif inRange(cX,cY,xrange = [1225.,2530.], yrange = [1380.,1460.]):
                print("debug: center (unmatched)",cX,cY, "\t area",cv2.contourArea(cnt))
            else:
                pass
                # print("debug: center (unmatched)",cX,cY, "\t area",cv2.contourArea(cnt))


    cnts = cnts_sel

    cnts = sorted(cnts, key = cv2.contourArea, reverse = True)[:6]
    # show(thresh)
    # exit(0)

    for cnt in cnts:
        area = cv2.contourArea(cnt)
        M = cv2.moments(cnt)
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
        print("debug: area for selected cnt", cv2.contourArea(cnt),"center ",cX,cY)
        
    i=0
    edgePoints = []
    edgePointsY = []
    # loop over the contours
    # exit()
    for c in cnts:

        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])

        print("contour:",i,len(c))
        i=i+1
        ip=0
        yarray = []
        points_array = []
        for point in c:
            # print(ip,"p",point,point[0][1])
            ip = ip + 1
            yarray.append(point[0][1])
            
        ymin_index = yarray.index(min(yarray))
        # print("point",ymin_index, "have min y. ",c[ymin_index])

        # index = ymin_index

        alignY = 0.
        # For USTC        
        # if vendor == "USTC" and (c[ymin_index][0][1] < 1200 or c[ymin_index][0][1] > 1530): 
        if vendor == "USTC" and (c[ymin_index][0][1] < 1350 or c[ymin_index][0][1] > 1455): 
            continue

        # For IHEP
        # if vendor == "IHEP" and (c[ymin_index][0][1] < 1280 or c[ymin_index][0][1] > 1380): 
        if vendor == "IHEP" and (c[ymin_index][0][1] < 1350 or c[ymin_index][0][1] > 1455): 
            continue

        # if vendor == "IHEP":
        #     if inRange(cX,cY,xrange = [1832.,1932.], yrange = [1380.,1460.]): alignY = 5.
        #     if inRange(cX,cY,xrange = [1382.,1472.], yrange = [1380.,1460.]): alignY = 5.

        # c[ymin_index][0][1] = c[ymin_index][0][1] + alignY

        edgePoints.append(c[ymin_index])
        edgePointsY.append(c[ymin_index][0][1])



    print(edgePointsY)
    edgePointsYSort = sorted(edgePointsY)
    print(edgePointsYSort)
    # exit(0)
    # 100 pixel = 160 um
    while len(edgePointsYSort) > 2 and abs(edgePointsYSort[-1] - edgePointsYSort[-2]) > 100:
        edgePoints.pop(edgePointsY.index(edgePointsYSort[-1]))
        edgePointsY.pop(edgePointsY.index(edgePointsYSort[-1]))
        edgePointsYSort = sorted(edgePointsY)

    while len(edgePointsYSort) > 2 and abs(edgePointsYSort[0] - edgePointsYSort[1]) > 100:
        edgePoints.pop(edgePointsY.index(edgePointsYSort[0]))
        edgePointsY.pop(edgePointsY.index(edgePointsYSort[0]))
        edgePointsYSort = sorted(edgePointsY)

    if len(edgePoints) < 4:
        print("Warning!!: Rotation aligment is not accurate, only ",len(edgePointsYSort)," points used")
        log_info(f"Warning!!: Rotation aligment is not accurate, only {len(edgePointsYSort)} points used")
    
    if len(edgePoints) < 2:
        print("Warning!!: < 2 points for aligment obtained, retaking the photo with +2 lumi")
        log_info(f"Warning!!: < 2 points for aligment obtained, retaking the photo with +2 lumi")
        # if lumi_now < 36: ##MaxLumi 36
        if lumi_now < 18: ##MaxLumi 36
            lumi_now = lumi_now + 2
            print("Setting lumi to ",lumi_now,"...")
            log_info(f"Setting lumi to {lumi_now}...")
        prober.vision.camera.set_light(CameraMountPoint.Scope, lumi_now)
        return -99.0

    x = []
    y = []

    print("edgePoints available:")
    for ep in edgePoints:
        print(ep)
        alignY = 0
        if vendor == "IHEP":
            if inRange(ep[0][0],ep[0][1],xrange = [1832.,1932.], yrange = [1360.,1460.]): alignY = 5
            if inRange(ep[0][0],ep[0][1],xrange = [1382.,1472.], yrange = [1360.,1460.]): alignY = 5

        x.append(ep[0][0])
        y.append(ep[0][1] + alignY)
        image = cv2.circle(image, (ep[0][0],ep[0][1] + alignY), radius=5, color=(0, 0, 255), thickness=-1)

    xarry = np.array(x)
    yarry = np.array(y)	


    a, b = np.polyfit(xarry, yarry, 1)
    angle = np.arctan(a)*180/np.pi # in degree

    print("y = ",a, "x + ",b)
    print("angle = ",angle, " degree")

    log_info(f"y = {a}*x + {b},  angle = {angle} degree, npoints:{len(edgePoints)}, lumi:{lumi_now}")
    log_info(f"edgepoints: x:{str(x)}, y:{str(y)}")

    cv2.imwrite("imgs/img_rot_fit.jpg", image)

    return angle


def get_label(threshpix=15):
    print("debug: start get label")
    time.sleep(0.5)
    os.system("wget -q -O "+base_path+"/img_label_now.jpg http://"+mpi_addr+"/qc-ts-img/img_label_now.jpg")


    image = cv2.imread("imgs/img_label_now.jpg")

    x = 1251
    y = 1297
    w = 1312
    h = 123

    # crop = image[y:y+h, x:x+w]
    crop = image
    # cv2.imshow('crop', crop)
    cv2.imwrite('imgs/img_label_crop.jpg', crop)

    gray = cv2.cvtColor(crop, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    thresh = cv2.threshold(blurred, 150, 255, cv2.THRESH_BINARY)[1]

    outpath="imgs/img_label_grey.jpg"
    cv2.imwrite(outpath, thresh)

    # find contours in the thresholded image
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)


    cnts_sel = []
    for cnt in cnts:
        area = cv2.contourArea(cnt)
        # print("debug:area for cnt", cv2.contourArea(cnt))
        
        if area > 1800 and area < 3000:
            M = cv2.moments(cnt)
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])

            if cX > 600 and cX < 2000:
                cnts_sel.append(cnt)
                # print("center ",cX,cY)		

    cnts = cnts_sel

    cnts = sorted(cnts, key = cv2.contourArea, reverse = True)
    # show(thresh)
    print("debug: getlabel: p1")

    for cnt in cnts:
        area = cv2.contourArea(cnt)
        # print("area for selected cnt", cv2.contourArea(cnt))
        
    i=0
    edgePoints = []
    edgePointsY = []

    # loop over the contours
    for c in cnts:
        # print("contour:",i,len(c))
        i=i+1
        ip=0
        yarray = []
        points_array = []
        for point in c:
            ip = ip + 1
            yarray.append(point[0][1])
            
        ymin_index = yarray.index(min(yarray))
        # print("point",ymin_index, "have min y. ",c[ymin_index])

        index = ymin_index
        edgePoints.append(c[ymin_index])
        edgePointsY.append(c[ymin_index][0][1])
        
    # print(edgePointsY)
    edgePointsYSort = sorted(edgePointsY)
    # print(edgePointsYSort)

    while len(edgePointsYSort) > 2 and abs(edgePointsYSort[-1] - edgePointsYSort[-2]) > 100:
        edgePoints.pop(edgePointsY.index(edgePointsYSort[-1]))
        edgePointsY.pop(edgePointsY.index(edgePointsYSort[-1]))
        edgePointsYSort = sorted(edgePointsY)

    while len(edgePointsYSort) > 2 and abs(edgePointsYSort[0] - edgePointsYSort[1]) > 100:
        edgePoints.pop(edgePointsY.index(edgePointsYSort[0]))
        edgePointsY.pop(edgePointsY.index(edgePointsYSort[0]))
        edgePointsYSort = sorted(edgePointsY)

    contoursSelected = []
    for c in cnts:
        yarray = []
        points_array = []
        for point in c:
            yarray.append(point[0][1])
            
        ymin_index = yarray.index(min(yarray))
        
        if c[ymin_index][0][1] in edgePointsY:
            contoursSelected.append(c)

    print("contoursSelected",len(contoursSelected))
    if len(contoursSelected) != 15:
        # status -2: not enough label recognized, need to enhance the light
        return -2,False,False,False

    # if len(edgePoints) < 4:
    #     print("Warning!!: Rotation aligment is not accurate, only ",len(edgePointsYSort)," points used")

    x = []
    y = []

    # print("edgePoints available:")
    for ep in edgePoints:
        # print(ep)
        x.append(ep[0][0])
        y.append(ep[0][1])
        crop = cv2.circle(crop, (ep[0][0],ep[0][1]), radius=5, color=(0, 0, 255), thickness=-1)

    xarry = np.array(x)
    yarry = np.array(y)	


    cv2.drawContours(crop, contoursSelected, -1, (0,255,0), 3)
    cv2.drawContours(thresh, contoursSelected, -1, (0,255,0), 3)

    mask = np.ones(thresh.shape[:2], dtype="uint8") * 0


    icnt = 0
    andcontourMap = {}
    maskMap = {}

    labelXID = {} # [x position: bit value]
    labelXIDCnt = {}
    for cnt in contoursSelected:
        maskMap[icnt] = np.ones(thresh.shape[:2], dtype="uint8") * 0
        cv2.drawContours(maskMap[icnt], [cnt], -1, 255, -1)

    # andcontour=
        andcontourMap[icnt] = cv2.bitwise_and(thresh, thresh, mask=maskMap[icnt])

        M = cv2.moments(cnt)
        cX = int(M["m01"] / M["m00"])
        cY = int(M["m10"] / M["m00"])
        
        sizeROI = int(10/pixelsize) # um , the width and height would be 2*sizeROI
        xlow = cX - sizeROI
        xhi =  cX + sizeROI
        ylow = cY - sizeROI
        yhi =  cY + sizeROI
        # print("debug>>",cX,cY,xlow,xhi,ylow,yhi)

        counterPixelBlack = 0
        for ypixel in range(ylow, yhi):
            for xpixel in range(xlow, xhi):
                if andcontourMap[icnt][xpixel, ypixel] == 0:
                    counterPixelBlack = counterPixelBlack + 1
                    # andcontourMap[icnt][xpixel, ypixel] = 255

        re = 0

        # cv2.imwrite("imgs/img_label_pad{:d}.jpg".format(icnt), andcontourMap[icnt])
        
        icnt = icnt + 1 

        if re == 1:
            cv2.drawContours(crop, [cnt], -1, (0,0,255), -1)

    # andcontour = cv2.bitwise_and(thresh, thresh, mask=mask)

    cv2.imwrite("imgs/img_label_crop_fit.jpg", crop)
    cv2.imwrite("imgs/img_label_grey_fit.jpg", thresh)
    cv2.imwrite("imgs/img_label_mask.png", mask)

    ibit = 0
    labelID = []
    for xpos in labelXIDKey:
        print("debug","bit",ibit,":",labelXID[xpos],labelXIDCnt[xpos])
        ibit = ibit + 1
        labelID.append(labelXID[xpos])

    nrow = labelID[0]*4 + labelID[1]*2 +  labelID[2]*1 
    ncol = labelID[3]*4 + labelID[4]*2 +  labelID[5]*1 
    nwafer = labelID[6]*32 + labelID[7]*16 +  labelID[8]*8 + labelID[9]*4  + labelID[10]*2  + labelID[11]*1 

    #if nwafer == 2 and ncol == 2 and nrow == 0:
     #   nrow = nrow + 4
    #if nwafer == 2 and ncol == 6 and nrow == 0:
     #   nrow = nrow + 4
    #if nwafer == 2 and ncol == 7 and nrow == 0:
     #   nrow = nrow + 4
    
    #if nwafer == 2 and nrow == 0:
     #   nrow = nrow + 4

    
    print(">>> obtained label,  wafer:{:d} row:{:d} col:{:d} pid:{:d}".format(nwafer,nrow,ncol,getPID(nrow,ncol)))

    os.system("cp imgs/img_label_now.jpg imgs/label_archive/img_label_Die{:d}_W{:d}-P{:d}_{:d}-{:d}.jpg".format(ndie,nwafer,getPID(nrow,ncol),nrow,ncol))
    os.system("cp imgs/img_now.jpg imgs/aligment_archive/img_Die{:d}_W{:d}-P{:d}_{:d}-{:d}.jpg".format(ndie,nwafer,getPID(nrow,ncol),nrow,ncol))


    print("debug: end get label")
    if nwafer in [2,12,24] and getPID(nrow,ncol) in range(0,53):
        os.system("touch label_status/align_{:d}_die-{:d}_W{:d}-P{:d}_r{:d}c{:d}_SUCCESS.txt".format(int(time.time()),ndie,nwafer,getPID(nrow,ncol),nrow,ncol))
    else:
        os.system("touch label_status/align_{:d}_die-{:d}_W{:d}-P{:d}_r{:d}c{:d}_FAILED.txt".format(int(time.time()),ndie,nwafer,getPID(nrow,ncol),nrow,ncol))
        # -1: no validate wafer id but the label pad is 15    
        return -1,False,False,False   


    # for ipic in [1,2,3,4,5,6,7]:

    #     os.system("wget -q -O "+base_path+"/snapshot_archive/img_Die{:d}_W{:d}-P{:d}_{:d}-{:d}.p{:d}.jpg http://{}/qc-ts-img/img_now_p{:d}.jpg".format(ndie,nwafer,getPID(nrow,ncol),nrow,ncol,ipic,mpi_addr,ipic) )
    #     time.sleep(0.5)

    
    return 1, nwafer, nrow, ncol    

def getValCmd(cmd):
    
    rtn = subprocess.check_output(cmd,shell=True).rstrip() 
    print("cmd:",cmd,"rtn",rtn)
    if len(rtn) > 0 and b'None' not in rtn:
        rtn = float(rtn)
    else:
        rtn = -1.
    return rtn

def call_daq(nwafer, pid):
    global picfolder_name, test_batch, die_pos_now, holder_id
    print("start daq for wafer",nwafer, " pid ",pid)
    log_info(f"start daq for wafer {nwafer} pid {pid}")

    prober.move_chuck_contact()
    time.sleep(0.5)

    prober.vision.snap_image(mpi_img_path+"\img_now_contact.jpg")  

    time.sleep(0.5)

    prober.vision.camera.set_light(CameraMountPoint.Scope, 0)
    time.sleep(0.5)

    temperature = prober.status.get_chuck_temp()

    cleanroom_T = getValCmd("cat data_cleanroom_environment.txt |sed 's/<[^>]*>//g' |grep temperature -A 1|tail -n 1")
    cleanroom_H = getValCmd("cat data_cleanroom_environment.txt |sed 's/<[^>]*>//g' |grep relative_humidity -A 1|tail -n 1")

    cleanroom_PC05 = getValCmd("cat data_cleanroom_particles.txt|sed 's/<[^>]*>//g' |grep part_count -A 1|tail -n 1|awk -F ',' '{print $2}'")
    cleanroom_PC25 = getValCmd("cat data_cleanroom_particles.txt|sed 's/<[^>]*>//g' |grep part_count -A 1|tail -n 1|awk -F ',' '{print $4}'")
    cleanroom_ISO05 = getValCmd("cat data_cleanroom_particles.txt|sed 's/<[^>]*>//g' |grep iso -A 1|tail -n 1|awk -F ',' '{print $2}'")
    cleanroom_ISO25 = getValCmd("cat data_cleanroom_particles.txt|sed 's/<[^>]*>//g' |grep iso -A 1|tail -n 1|awk -F ',' '{print $4}'")

    yml_cmd = f"cat CERN_setup_tpl.txt.yml"

    yml_cmd = yml_cmd +  f"|sed  's/Serial Number: 0/Serial Number: {nwafer}/g'"
    yml_cmd = yml_cmd +  f"|sed  's/Position: 99/Position: {pid}/g'"
    yml_cmd = yml_cmd +  f"|sed  's/Vendor: USTC-IME/Vendor: {vendor}-IME/g'"
    yml_cmd = yml_cmd +  f"|sed 's/Temperature: 0.0/Temperature: {temperature}/g'"
    yml_cmd = yml_cmd +  f"|sed  's/PATH_TO_SNAPSHOT_FOLDER/{picfolder_name}/g'"


    yml_cmd = yml_cmd +  f"|sed  's/Cleanroom_Humidity: 0.0/Cleanroom_Humidity: {cleanroom_H}/g'"
    yml_cmd = yml_cmd +  f"|sed  's/Cleanroom_Temperature: 22.0/Cleanroom_Temperature: {cleanroom_T}/g'"
    yml_cmd = yml_cmd +  f"|sed  's/Cleanroom_PC05: 0.0/Cleanroom_PC05: {cleanroom_PC05}/g'"
    yml_cmd = yml_cmd +  f"|sed  's/Cleanroom_PC25: 0.0/Cleanroom_PC25: {cleanroom_PC25}/g'"
    yml_cmd = yml_cmd +  f"|sed  's/Cleanroom_ISO05: 0.0/Cleanroom_ISO05: {cleanroom_ISO05}/g'"
    yml_cmd = yml_cmd +  f"|sed  's/Cleanroom_ISO25: 0.0/Cleanroom_ISO25: {cleanroom_ISO25}/g'"
    
    yml_cmd = yml_cmd +  f"|sed  's/AutoTest_Batch_ID: -1/AutoTest_Batch_ID: {test_batch}/g'"
    yml_cmd = yml_cmd +  f"|sed  's/Holder_ID: -1/Holder_ID: {holder_id}/g'"
    yml_cmd = yml_cmd +  f"|sed  's/Holder_Die_Position: -1_-1/Holder_Die_Position: {die_pos_now}/g'"

    yml_cmd = yml_cmd + f" > Quene/CERN_setup_batch{test_batch}_W{nwafer}P{pid}.yml"
    print("Generated yml:",yml_cmd)
    log_info(f"Generated yml:{yml_cmd}")
    #.format(nwafer,pid,vendor,temperature, picfolder_name , nwafer,pid)

    os.system(yml_cmd)
    
    while True:
        if os.path.isfile("Jobs/CERN_setup_batch{}_W{:d}P{:d}.yml.done".format(test_batch,nwafer,pid)):
            print("The job for batch{} W{:d}P{:d} is finished!!!".format(test_batch,nwafer,pid))
            break
        else:
            print("The job for batch{} W{:d}P{:d} is running.... waiting in 20s".format(test_batch,nwafer,pid))
            time.sleep(20)


    prober.vision.camera.set_light(CameraMountPoint.Scope, lumi)

    prober.vision.snap_image(mpi_img_path+"\img_now_contact_post.jpg")  

    prober.move_chuck_separation()

    log_info(f"finished daq for wafer {nwafer} pid {pid}")



def run_test(die_pos=[0,0]):
    status = 0 
    global lumi_now, picfolder_name, die_pos_now

    lumi_now = lumi

    die_pos_now = "{}_{}".format(die_pos[0],die_pos[1])

    print("testing the die",die_pos,"......")
    log_info(f"testing the die {die_pos}......")

    nwafer = HolderPositionMap[die_pos_now][0] # from holder position to get the device info
    pid    = HolderPositionMap[die_pos_now][1]

    print("nwafer ",nwafer,"pid",pid)

    # theta_threshold = 0.03
    theta_threshold = 0.10
    # return status: 0: untested, 1 test succeed 2 test skipped due to aligment failed

    aligmentOK = False
    # Prepare communication
    nAttempt = 1
    if not comm or not prober:
        print("Probe station not connected!! code exit...")
        return -1
    
    prober.move_chuck_separation()
    prober.vision.camera.set_light(CameraMountPoint.Scope, lumi)

    
    while aligmentOK == False:
        print("                              ")
        log_info(f"Preparing the {nAttempt} attempt for aligment")
        if nAttempt > 15:
        # if nAttempt > 5:
        # if nAttempt > 5:
            print(f"Failed to align after {nAttempt-1}th aligment, going to exit....")
            log_info(f"Failed to align after {nAttempt-1}th aligment, going to exit....")
            status = 2
            break
        nAttempt = nAttempt + 1

        pos_now = prober.get_chuck_xy(ChuckSite.Wafer,ChuckXYReference.User)
        time.sleep(0.2)
        prober.vision.snap_image(mpi_img_path+"\img_now.jpg")
        log_info("start align x,y")
        shift = align()
        log_info(f"a.{nAttempt}.0 x,y shift {shift[0]},{shift[1]} ")

        pos_target = [ pos_now[0] - shift[0],pos_now[1] + shift[1]]

        print("current position: X:{:f} Y:{:f}".format(pos_now[0],pos_now[1]))
        print("target position: X:{:f} Y:{:f}".format(pos_target[0],pos_target[1]))    
        
        x = pos_target[0]
        y = pos_target[1]
        
        # prober.move_chuck_xy(ChuckXYReference.User, x, y)

        prober.move_chuck_xy(ChuckXYReference.User, x, y - 200) # move to 200 um lower for rotation correction
        time.sleep(0.2)
        prober.vision.snap_image(mpi_img_path+"\img_now_theta.jpg")
        prober.move_chuck_xy(ChuckXYReference.User, x, y)
        # theta aligment
        theta_shift = align_theta()

        # if abs(theta_shift) > 5.0:

        log_info(f"a.{nAttempt} theta_shift:{theta_shift}")
        # if abs(theta_shift) > 0.7:
        if abs(theta_shift) > 1.5:
            log_info(f"invalidate theta value {theta_shift}, require: <1.5, retrying the aligment....")
            continue
        
        if abs(theta_shift) > theta_threshold:
            theta_now = prober.get_chuck_theta(ChuckSite.Wafer)
            print("theta now",theta_now)
            theta_target =  theta_now + theta_shift
            print("theta_target",theta_target)
            prober.move_chuck_theta(ChuckThetaReference.Site, theta_target)


        theta_now = prober.get_chuck_theta(ChuckSite.Wafer)

        # Post Theta XY aligment
        pos_now = prober.get_chuck_xy(ChuckSite.Wafer,ChuckXYReference.User)
        time.sleep(0.2)
        prober.vision.snap_image(mpi_img_path+"\img_now.jpg")
        log_info("start post-theta align x,y")
        shift = align()
        log_info(f"a.{nAttempt}.1 x,y shift {shift[0]},{shift[1]} ")
        pos_target = [ pos_now[0] - shift[0],pos_now[1] + shift[1]]
        print("current position: X:{:f} Y:{:f}".format(pos_now[0],pos_now[1]))
        print("target position: X:{:f} Y:{:f}".format(pos_target[0],pos_target[1]))    
        x = pos_target[0]
        y = pos_target[1]
        prober.move_chuck_xy(ChuckXYReference.User, x, y)



        log_info(f"a.{nAttempt} ==  Aligment Quality: dX: {shift[0]:f} um, dY: {shift[1]:f} um, dTheta: {theta_shift:f} deg, theta:{theta_now}, lumi:{lumi_now}")
        
        if abs(shift[0]) < 5 and abs(shift[1]) < 5 and abs(theta_shift) < theta_threshold:
            aligmentOK = True
        print("!!!!!! aligment status: ",aligmentOK," !!!!!")

        log_info(f"==  Aligment status: {aligmentOK} attempt:{nAttempt} lumi:{lumi_now}")

        time.sleep(1)

    print("aligment status ", aligmentOK)


    if aligmentOK:

        os.system("touch align_status/align_{:d}_die-{:d}_SUCCESS_A{:d}.txt".format(int(time.time()),ndie,nAttempt))
        log_info("==  generating test file: ./align_status/align_{:d}_die-{:d}_SUCCESS_A{:d}.txt".format(int(time.time()),ndie,nAttempt))
        # # Move to label 

        # time.sleep(0.5)

        #Save the photos
        prober.move_chuck_xy(ChuckXYReference.User, x + 1000, y - 100)
        time.sleep(0.5)
        prober.vision.snap_image(mpi_img_path+"\img_now_p1.jpg")

        prober.move_chuck_xy(ChuckXYReference.User, x - 2000, y - 100)
        time.sleep(0.5)
        prober.vision.snap_image(mpi_img_path+"\img_now_p2.jpg")        

        prober.move_chuck_xy(ChuckXYReference.User, x - 5000, y - 100)
        time.sleep(0.5)
        prober.vision.snap_image(mpi_img_path+"\img_now_p3.jpg") 


        prober.move_chuck_xy(ChuckXYReference.User, x - 8000, y - 100)
        time.sleep(0.5)
        prober.vision.snap_image(mpi_img_path+"\img_now_p4.jpg") 


        prober.move_chuck_xy(ChuckXYReference.User, x - 11000, y - 100)
        time.sleep(0.5)
        prober.vision.snap_image(mpi_img_path+"\img_now_p5.jpg") 


        prober.move_chuck_xy(ChuckXYReference.User, x - 14000, y - 100)
        time.sleep(0.5)
        prober.vision.snap_image(mpi_img_path+"\img_now_p6.jpg")        

        prober.move_chuck_xy(ChuckXYReference.User, x - 17000, y - 100)
        time.sleep(0.5)
        prober.vision.snap_image(mpi_img_path+"\img_now_p7.jpg")  

        prober.move_chuck_xy(ChuckXYReference.User, x + 1395.0, y - 800.0)
        time.sleep(0.5)
        prober.vision.snap_image(mpi_img_path+"\img_now_label.jpg")  

        prober.move_chuck_xy(ChuckXYReference.User, x, y)



        pid = -1


        # if vendor in ["USTC"]:
        if vendor in []:
            for nAttemptLabel in range(0,8): 
                
                lumi_now = 10 + 3*nAttemptLabel # from 10 to 31

                print("Setting lumi to ",lumi_now,"...")          
                prober.vision.camera.set_light(CameraMountPoint.Scope, lumi_now)
                prober.move_chuck_xy(ChuckXYReference.User, x + 1395.0, y - 800.0)
                time.sleep(0.2)
                prober.vision.snap_image(mpi_img_path+"\img_label_now.jpg")
                
                status_label, nwafer, nrow, ncol  = get_label()
                if status_label != -2:
                    breakTemperature: 25.0
            
            if status_label == -1:
                for nAttemptLabel in range(0,16): 
                    thresh_now = 8 + 2*nAttemptLabel 
                    status_label, nwafer, nrow, ncol  = get_label(threshpix=thresh_now)
                    if status_label != -1:
                        break


            if status_label == -1:
                    nwafer = 0
                    nrow = -1
                    pid = -1

            pid = getPID(nrow,ncol)

        elif vendor in ["IHEP","USTC"]:

            nwafer = -1
            nrow = -1
            ncol = -1


            nwafer = HolderPositionMap[die_pos_now][0] # from holder position to get the device info
            pid    = HolderPositionMap[die_pos_now][1]
            lumi_now = lumi

        else:
            print("Error!! wrong vendor fond, please check..... vendor:",vendor)
            exit(0)


        dateid = int((time.mktime((datetime.now()).timetuple())))

        ## Saving plots

        picfolder_name = "snapshot_{}_V-{}_W{}-P{}_D-{}".format(dateid,vendor,nwafer,pid,die_pos_now)

        picfolder = base_path + "/" + picfolder_name
        
        os.system("mkdir -p {}".format(picfolder))

        for ipic in ["img_now_p1","img_now_p2","img_now_p3","img_now_p4","img_now_p5","img_now_p6","img_now_p7","img_now_label"]:    
            
            picpath = "{}/{}.jpg".format(picfolder,ipic)

            sourcepath = "http://{}/qc-ts-img/{}.jpg".format(mpi_addr,ipic)

            print("[Camera] saving {} to {} ......".format(sourcepath, picpath) )
            os.system("wget -q -O {} {}".format(picpath, sourcepath) )
            time.sleep(0.1)

        
        prober.move_chuck_xy(ChuckXYReference.User, x, y)
        
        print("Going to measure the device for wafer {:d}, pid {:d}".format(nwafer,pid))
        
        time.sleep(0.5)
        
        call_daq(nwafer,pid)

        prober.move_chuck_separation()


        ## Saving plots
        for ipic in ["img_now_contact","img_now_contact_post"]:

            picpath = "{}/{}.jpg".format(picfolder,ipic)

            sourcepath = "http://{}/qc-ts-img/{}.jpg".format(mpi_addr,ipic)

            print("[Camera] saving {} to {} ......".format(sourcepath, picpath) )
            os.system("wget -q -O {} {}".format(picpath, sourcepath) )
            time.sleep(0.1)


        return 1
    else:
        os.system("touch align_status/align_{:d}_die-{:d}_FAILED.txt".format(int(time.time()),ndie))
        print("the aligment is failed for this one, it will be skipped and we will go to the next one")

        log_info("==  skip failed aligment: ./align_status/align_{:d}_die-{:d}_SUCCESS_A{:d}.txt".format(int(time.time()),ndie,nAttempt))
        return 2

    


def main():
    global comm, prober,ndie

    comm = CommunicatorTcpIp()
    comm.connect( mpi_addr + ":35555")

    # you can also use the prober to send commands
    prober = SentioProber(comm)
    prober.comm.send("*IDN?")
    # prober.comm.send("*LOCAL")
    # return 0
    print(comm.read_line())

    print("Start stepping of the qc test structures.....")
    prober.map.bins.clear_all()

    col, row, site  = prober.map.step_first_die()
    ndie=1

    dieList = []

    test_status = -1

    print("stepping first die")
    time.sleep(1)
    
    if len(dieList) > 0 and ndie not in dieList:
        pass
    elif test_device and f'{col}_{row}' != test_device:
        pass
    elif check_history(col,row):
        log_info(f'{col}_{row} already tested, continue....')
        pass
    else:
        time.sleep(5)
        # test_status = 1
        test_status = run_test([col,row])
        record_test(f'{col},{row},{test_status}')

    bin_value = 3

    

    try:
        while True:
            ndie=ndie+1
            bin_value = 3
            if test_status == 1:
                bin_value = 1
            if test_status == 2:
                bin_value = 0

            col, row, site = prober.map.bin_step_next_die(bin_value)
            
            if len(dieList) > 0 and ndie not in dieList:
                continue 

            if test_device and f'{col}_{row}' != test_device:
                continue
            if check_history(col,row):
                log_info(f'{col}_{row} already tested, continue....')
                continue
            # if ndie < 7 :
            # continue
            print(f'Position {col}, {row} (Site: {site})')
            print("stepping new die")
            time.sleep(1)

            # test_status = 1
            test_status = run_test([col,row])
            record_test(f'{col},{row},{test_status}')


    except ProberException as e:
        if e.error() != RemoteCommandError.EndOfRoute:
            raise
    except KeyboardInterrupt as e:
        print('====== User KeyboardInterrupted======')
        print('Will do nothing, please terminate the DAQ measurement manually before moving the needle!!!')
        print('exiting .......')
        prober.comm.send("*LOCAL")
        exit(0)


    prober.comm.send("*LOCAL")
    print("Finished stepping of the qc test structures.....")

if __name__ == "__main__":
    try:
        import argparse

        parser = argparse.ArgumentParser(description='Process some integers.')
        parser.add_argument('-b','--batch',type=int, dest='batch', default=-1,help='Set test batch number by force')
        parser.add_argument('-d','--device',type=str, dest='device', default=None,help='Set the device to test col,row ')

        args = parser.parse_args()
        if args.batch > 0:
            test_batch = args.batch
            print(f"Using the test batch(set by force): {test_batch}")
        else:
            print(f"Using the test batch: {test_batch}")
        if args.device:
            test_device = args.device
            test_device = test_device.strip()
            print(f"Only test the device col,row (set by force): '{test_device}'")
        else:
            print("Testing all the device....")
        #exit(0)
        record_file = f'./job_record/job_record_{test_batch}.txt'
        # print(getPID(2,4))
        main()
    except Exception as e:
        print("\n#### Error ####")
        print("{0}".format(e))
