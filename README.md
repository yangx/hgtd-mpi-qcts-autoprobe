
Dependency:

    MPI probe control: https://github.com/SentioProberDev/SentioProberControl

    OpenCV: https://pypi.org/project/opencv-python/

    Python3

Install:
    git clone https://gitlab.cern.ch/yangx/hgtd-mpi-qcts-autoprobe.git

    pip install sentio-prober-control 

    pip install opencv-python imutils

